# migration repo settings
default['gitlab_migration']['migration_repo']['git_ssh'] = 'git@dev.gitlab.org:gitlab-com/migration'
default['gitlab_migration']['migration_repo']['base_dir'] = '/opt/gitlab-migration'
default['gitlab_migration']['migration_repo']['deploy_key_fname'] = "#{node['gitlab_migration']['migration_repo']['base_dir']}/deploy_key"
default['gitlab_migration']['migration_repo']['path'] = "#{node['gitlab_migration']['migration_repo']['base_dir']}/migration"
default['gitlab_migration']['migration_repo']['user'] = 'root'
default['gitlab_migration']['migration_repo']['deploy_key'] = 'set in gkms/vault'
default['gitlab_migration']['migration_repo']['revision'] = 'master'
