# encoding: UTF-8

# Cookbook Name:: gitlab_migration
# Recipe:: default
# License:: MIT
#
# Copyright 2018, GitLab Inc.

secrets = get_secrets(node['gitlab_migration']['secrets']['backend'],
                      node['gitlab_migration']['secrets']['path'],
                      node['gitlab_migration']['secrets']['key'])

directory node['gitlab_migration']['migration_repo']['base_dir'] do
  owner    'root'
  group    'root'
end

file node['gitlab_migration']['migration_repo']['deploy_key_fname'] do
  content secrets['deploy_key']
  owner 'root'
  group 'root'
  mode '0600'
end

git node['gitlab_migration']['migration_repo']['path'] do
  repository  node['gitlab_migration']['migration_repo']['git_ssh']
  revision    node['gitlab_migration']['migration_repo']['revision']
  user        node['gitlab_migration']['migration_repo']['user']
  action      :sync
  environment 'GIT_SSH_COMMAND' => "ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i #{node['gitlab_migration']['migration_repo']['deploy_key_fname']}"
end
