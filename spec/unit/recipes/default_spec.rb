# encoding: UTF-8

# Cookbook:: gitlab_migration
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'

describe 'gitlab_migration::default' do
  before do
    allow_any_instance_of(Chef::Recipe).to receive(:get_secrets)
      .with('fake_backend', 'fake_path', 'fake_key')
      .and_return('deploy_key' => 'somefakekey')
  end

  context 'when all attributes are default, on Ubuntu 16.04' do
    let(:chef_run) do
      ChefSpec::ServerRunner.new(platform: 'ubuntu',
                                 version: '16.04') do |node|
        node.normal['gitlab_migration'] = {
          'secrets' => {
            'backend' => 'fake_backend',
            'path' => 'fake_path',
            'key' => 'fake_key',
          },
        }
      end.converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates the base dir' do
      expect(chef_run).to create_directory('/opt/gitlab-migration').with(
        owner: 'root',
        group: 'root'
      )
    end

    it 'creates the deploy key' do
      expect(chef_run).to(render_file('/opt/gitlab-migration/deploy_key').with_content do |c|
        expect(c).to eq('somefakekey')
      end)
    end

    it 'syncs the scripts repo' do
      expect(chef_run).to sync_git('/opt/gitlab-migration/migration')
        .with(repository: 'git@dev.gitlab.org:gitlab-com/migration')
        .with(revision:   'master')
    end
  end
end
